import subprocess as sp
import os
from sys import argv

app_path = '/Users/navazalani/Library/Developer/Xcode/DerivedData/testApp-cnayflvkzvehvbawjjgqggkvmhfp/Build/Products/Debug-iphonesimulator/testApp.app'
build_dest = '/Users/navazalani/Work/RN-Test/sim-build/app.zip'
build_cmd = f"ditto -ck --sequesterRsrc --keepParent '{app_path}' '{build_dest}'"


def build_for_sim():
    # clean the simulator_buid folder
    print(sp.getoutput(f'rm -rf $(ls {build_dest})'))
    print(sp.getoutput(build_cmd))


if len(argv) == 1:
    # no args
    print("No arguments provided.\nNo action taken.\nUse --build flag to build ")
else:
    if argv[1] == '--build':
        build_for_sim()
