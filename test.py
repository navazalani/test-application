from appium import webdriver
from testing_configs import emulation
from time import sleep
import subprocess as sp

sim_build_cmd = "cd /Users/navazalani/Work/RN-Test/ && python3 ./sim_build.py --build"
sp.getoutput(sim_build_cmd)


driver = webdriver.Remote('http://0.0.0.0:4723/wd/hub', emulation)

sleep(5)

print(driver.page_source)
