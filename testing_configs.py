from env import app_home_sim

emulation = desired_caps = {
    'platformName': 'iOS',
    'platformVersion': '12.2',
    'deviceName': 'iPhone Simulator',
    'app': app_home_sim,
    'automationName': 'XCUITest',
    'newCommandTimeout': 1000,
    'useNewWDA': True
}

# These capabilities have been verified to work on the device
# cl_iphone7 = {
#     # "app": "/Users/navazalani/Work/sessionz-tests/testing/SessionzStaging.app.zip",
#     "app": app_home_dev,
#     "automationName": "XCUITest",
#     "xcodeOrgId": "U2VJ4UCKR4",
#     "xcodeSigningId": "iPhone Developer",
#     "platformName": "iOS",
#     "platformVersion": "12.2",
#     "deviceName": "Crowdlinker's iPhone",
#     "udid": "55de45077c238fc67486590c14d2bcb345d906bb",
#     "noReset": "true"
# }

# cl_iphone6plus = {
#     # "app": "/Users/navazalani/Work/sessionz-tests/testing/SessionzStaging.app.zip",
#     "app": app_home_dev,
#     "automationName": "XCUITest",
#     "xcodeOrgId": "U2VJ4UCKR4",
#     "xcodeSigningId": "iPhone Developer",
#     "platformName": "iOS",
#     "platformVersion": "12.3.1",
#     "deviceName": "Crowdlinker's iPhone",
#     "udid": "7c1bfcc9e289c766bc1113de245eeedc4f7404bc",
#     "noReset": "true"
# }


# navaz_iphone8plus = {
#     # "app": "/Users/navazalani/Work/sessionz-tests/testing/SessionzStaging.app.zip",
#     "app": app_home_dev,
#     "automationName": "XCUITest",
#     "xcodeOrgId": "U2VJ4UCKR4",
#     "xcodeSigningId": "iPhone Developer",
#     "platformName": "iOS",
#     "platformVersion": "13.0",
#     "deviceName": "Navaz's iPhone",
#     "udid": "21429f4cfcf199f739eabd794969b4cfeea530a0",
#     "noReset": "true"
# }
